import { Project } from './Project';
import { Repository } from './Repository';
import { Commit } from './Commit';

export interface Tag {
    object_kind: string;
    before: string;
    after: string;
    ref: string;
    checkout_sha: string;
    user_id: 1;
    user_name: string;
    user_avatar: string;
    project_id: number;
    project: Project;
    repository: Repository;
    commits: Commit[];
    total_commits_count: number;
}
