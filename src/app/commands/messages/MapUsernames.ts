import { Message } from 'discord.js';
import { AbstractMessageCommand } from './AbstractMessageCommand';
import App from '../../App';
import { MapUsernames as MapUsernamesRepository } from '../../../database/Repositories/MapUsernames';

const COMMAND = '!map usernames';

export class MapUsernames extends AbstractMessageCommand {
    isValid(message: Message): boolean {
        return message.content.startsWith(COMMAND);
    }

    async handle(message: Message): Promise<void> {
        const parameters = message.content.replace(COMMAND, '').trim();
        const [gitlabUsername, discordUsername] = parameters.split('-');

        const db = await App.getDatabaseConnection();
        const repository = new MapUsernamesRepository(db);

        try {
            await repository.saveMap({
                gitlab_username: gitlabUsername,
                discord_username: discordUsername,
            });
        } catch (e) {
            await message.reply(`Cannot mapping usernames: ${e}`);
            return;
        }

        await message.reply(`Success mapped user ${gitlabUsername} - ${discordUsername}`);
    }
}
