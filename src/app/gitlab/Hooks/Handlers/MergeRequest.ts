import { AbstractHandler } from './AbstractHandler';
import { Request } from 'express';
import { MergeRequest as MergeRequestEntity } from '../Entities/MergeRequest';
import { EmbedFieldData, MessageEmbed, MessageOptions } from 'discord.js';
import { TimeHelper } from '../../../helpers/Time';

export class MergeRequest extends AbstractHandler {
    async handle(request: Request): Promise<void> {
        const channel = this.botClient.channels.cache.find((ch) => ch.id === this.appConfig.notificationChannelId);
        if (!channel?.isText()) {
            console.error('Undefined notification channel');
            return;
        }

        const body = request.body as MergeRequestEntity;

        let assigneeUsers = '';

        let i = 1;

        for (const index in body.assignees) {
            assigneeUsers += await this.getAssigneeUser(body.assignees[index].username);

            if (i !== body.assignees.length) {
                assigneeUsers += ', ';
            }

            i++;
        }

        const messageData: MessageOptions = {
            embeds: [await this.buildMessage(body)],
            content: assigneeUsers,
        };

        await channel.send(messageData);
    }

    private async buildMessage(body: MergeRequestEntity): Promise<MessageEmbed> {
        const [firstAssignee] = body.assignees;

        let assignee = '';

        if (firstAssignee) {
            assignee = await this.getAssigneeUser(firstAssignee.username);
        }

        const fields: EmbedFieldData[] = [];

        if (assignee !== '') {
            fields.push({
                name: 'assignee',
                value: assignee,
            });
            fields.push({
                name: '\u200B',
                value: '\u200B',
            });
        }

        fields.push(
            ...[
                { name: 'Branch src', value: body.object_attributes.source_branch, inline: true },
                { name: 'Branch target', value: body.object_attributes.target_branch, inline: true },
                { name: 'Created at', value: TimeHelper.dateToInternalFormat(body.object_attributes.created_at, true) },
            ],
        );

        const assigneeAvatarUrl =
            body.object_attributes.assignee && body.object_attributes.assignee.avatar_url
                ? body.object_attributes.assignee.avatar_url
                : '';

        const message = new MessageEmbed()
            .setColor('#0099ff')
            .setTitle(body.object_attributes.url)
            .setURL(body.object_attributes.url)
            .setAuthor(body.user.name, body.user.avatar_url)
            .setDescription(`Merge request in repository: ${body.repository.name} is ${body.object_attributes.state}`)
            .setThumbnail(body.project.avatar_url)
            .addFields(fields)
            .setTimestamp();

        if (assigneeAvatarUrl !== '') {
            message.setFooter(assignee, assigneeAvatarUrl);
        }

        return message;
    }
}
