export interface Commit {
    id: string;
    message: string;
    title: string;
    timestamp: string;
    url: string;
    author: {
        name: string;
        email: string;
    };
    added: string[];
    modified: string[];
    removed: string[];
}
