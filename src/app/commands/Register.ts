import { AbstractMessageCommand } from './messages/AbstractMessageCommand';
import { Hello } from './messages/Hello';
import { Client } from 'discord.js';
import { AbstractInteractionCommand } from './interaction/AbstractInteractionCommand';
import { DeleteRegisteredCommands } from './interaction/DeleteRegisteredCommands';
import App from '../App';
import { ShowRegisteredCommands } from './interaction/ShowRegisteredCommands';
import { ShowMergeRequests } from './messages/ShowMergeRequests';
import { GetChannelId } from './messages/GetChannelId';
import { ClearChannel } from './interaction/ClearChannel';
import { MapUsernames } from './messages/MapUsernames';
import { Help } from './interaction/Help';

export class CommandRegister {
    protected client: Client;

    constructor(client: Client) {
        this.client = client;
    }

    public run(): void {
        const appConfig = App.getAppConfig();
        const interactionCommands = this.getInteractionCommands();
        const messageCommands = this.getMessageCommands();

        this.client.on('ready', async () => {
            console.log(`Logged in as ${this.client.user?.tag}!`);

            for (const command of interactionCommands) {
                await this.client.application?.commands.create(command.createCommand(), appConfig.guildId);
            }
        });

        this.client.on('messageCreate', async (message) => {
            for (const command of messageCommands) {
                if (command.isValid(message)) {
                    await command.handle(message);
                    return;
                }
            }
        });

        this.client.on('interactionCreate', async (interaction) => {
            for (const command of interactionCommands) {
                if (command.isValid(interaction)) {
                    await command.handle(interaction);
                    return;
                }
            }
        });
    }

    private getMessageCommands(): AbstractMessageCommand[] {
        return [
            new Hello(this.client),
            new ShowMergeRequests(this.client),
            new GetChannelId(this.client),
            new MapUsernames(this.client),
        ];
    }

    private getInteractionCommands(): AbstractInteractionCommand[] {
        return [
            new Help(this.client),
            new DeleteRegisteredCommands(this.client),
            new ShowRegisteredCommands(this.client),
            new ClearChannel(this.client),
        ];
    }
}
