import { Database } from 'sqlite';
import { SimpleObject } from '../../contracts/ObjectTypes';

export interface Pagination<T> {
    totalCount: number;
    page: number;
    perPage: number;
    data: T[];
}

export abstract class AbstractRepository {
    protected db: Database;

    constructor(database: Database) {
        this.db = database;
    }

    abstract get tableName(): string;

    async getPaginatedData<T>(select: string[], sqlQuery: string, page = 1, perPage = 15): Promise<Pagination<T>> {
        const limit = perPage;
        const offset = limit * (page - 1);

        await this.db.run('BEGIN;');

        try {
            const countQuery = await this.db.get(`select count(*) as 'count' from ${this.tableName} ${sqlQuery}`);
            const rows = await this.db.all(
                `select ${select.join(',')} from ${this.tableName} ${sqlQuery} limit ${limit} offset ${offset};`,
            );

            await this.db.run('COMMIT;');
            return {
                totalCount: countQuery.count,
                page: page,
                perPage: perPage,
                data: rows,
            } as Pagination<T>;
        } catch (e) {
            await this.db.run('ROLLBACK');
            throw e;
        }
    }

    async save<T extends SimpleObject>(entity: T, fields: string[]): Promise<number> {
        let sql = `INSERT OR REPLACE INTO ${this.tableName} (${fields.join(',')}) VALUES(`;

        let counter = 1;
        for (const key of fields) {
            if (entity[key] || entity[key] === 0) {
                sql += `'${entity[key].toString()}'`;
            } else {
                sql += `''`;
            }
            if (counter < fields.length) {
                sql += ', ';
            }
            counter++;
        }
        sql += `);`;

        console.log(sql);

        const result = await this.db.run(sql);

        return Number(result.lastID);
    }
}
