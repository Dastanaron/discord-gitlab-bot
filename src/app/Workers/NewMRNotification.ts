import GitLabAPI from '../gitlab/Api/API';
import { Client } from 'discord.js';
import App from '../App';
import { MergeRequestRepository } from '../../database/Repositories/MergeRequest';
import { TimeHelper } from '../helpers/Time';

export class NewMRNotification {
    protected client: Client;
    protected channelId: string;

    constructor(client: Client) {
        const appConfig = App.getAppConfig();
        this.client = client;
        this.channelId = appConfig.notificationChannelId;
    }

    public async run(): Promise<void> {
        const db = await App.getDatabaseConnection();
        const mergeRequestRepository = new MergeRequestRepository(db);

        setInterval(async () => {
            const channel = this.client.channels.cache.find((ch) => ch.id === this.channelId);
            if (!channel?.isText()) {
                return;
            }

            const mergeRequests = await GitLabAPI.loadMergeRequests();
            for (const mergeRequest of mergeRequests) {
                const dbMergeRequest = await mergeRequestRepository.getByExternalId(mergeRequest.id);

                if (dbMergeRequest === null) {
                    let message = `${mergeRequest.web_url}\n**author**: ${mergeRequest.author.name}\n**assignee**: `;

                    if (mergeRequest.assignee) {
                        message += `${mergeRequest.assignee.username}\n`;
                    } else {
                        message += 'Not selected\n';
                    }

                    message += `branch ***${mergeRequest.source_branch}*** to ***${mergeRequest.target_branch}***\n`;

                    message += `**Created at**: ${TimeHelper.dateToInternalFormat(mergeRequest.created_at, true)}`;
                    channel.send(message);

                    await mergeRequestRepository.saveMR({
                        external_id: mergeRequest.id,
                        author: mergeRequest.author.username,
                        title: mergeRequest.title,
                        url: mergeRequest.web_url,
                        created_at: Math.round(new Date(mergeRequest.created_at).getTime() / 1000),
                        is_sent: 1,
                    });
                }
            }
        }, 5000);
    }
}
