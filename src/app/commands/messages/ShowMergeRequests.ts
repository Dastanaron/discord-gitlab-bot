import { Message } from 'discord.js';
import { AbstractMessageCommand } from './AbstractMessageCommand';
import GitLabAPI from '../../gitlab/Api/API';
import { MergeRequest } from '../../gitlab/Api/Entities/MergeRequest';

const COMMAND = '!show mr';

export class ShowMergeRequests extends AbstractMessageCommand {
    isValid(message: Message): boolean {
        return message.content.startsWith(COMMAND);
    }

    async handle(message: Message): Promise<void> {
        const user = message.content.replace(COMMAND, '').trim();
        const mergeRequests = await GitLabAPI.loadMergeRequests();

        const selectedRequests: MergeRequest[] = [];

        for (const mergeRequest of mergeRequests) {
            if (mergeRequest.assignee?.username === user) {
                selectedRequests.push(mergeRequest);
            }
        }

        if (selectedRequests.length === 0) {
            await message.reply(`Not found merge requests for user ${user}`);
            return;
        }

        let messageForUser = `Merge requests for user ${user}\n\n`;

        for (const mergeRequest of selectedRequests) {
            messageForUser += `${mergeRequest.web_url} - ${mergeRequest.author.name} | ${mergeRequest.created_at}\n`;
        }

        await message.reply(messageForUser);
    }
}
