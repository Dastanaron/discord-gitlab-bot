import { AbstractAPI } from './AbstractApi';
import HookHandler from '../gitlab/Hooks/Handler';
import { HTTPError } from '../errors/Errors';

export class WebHook extends AbstractAPI {
    public run(): void {
        this.gitlabWebHook();
    }

    private gitlabWebHook() {
        this.controller.post('/callback/gitlab', async (request, response) => {
            try {
                await HookHandler.run(request);
            } catch (error) {
                if (error instanceof HTTPError) {
                    this.errorResponse(response, error.message, error.statusCode);
                } else {
                    console.error(error);
                    this.errorResponse(response, 'Undefined error', 500);
                }
                return;
            }

            this.successResponse(response, [], 'ok');
        });
    }
}
