import App from './app/App';
import Controller from './app/controllers/Kernel';
import { CommandRegister } from './app/commands/Register';

(async () => {
    const appConfig = App.getAppConfig();
    const client = App.getBotClient();

    const commandRegister = new CommandRegister(client);
    commandRegister.run();

    await client.login(appConfig.botToken);

    Controller.run();
})();
