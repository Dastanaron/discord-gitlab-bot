import express, { Express } from 'express';
import App from '../App';
import { Common } from './Common';
import { WebHook } from './WebHook';

export class Kernel {
    private readonly controller: Express;

    constructor() {
        this.controller = express();
    }
    public run(): void {
        const appConfig = App.getAppConfig();
        this.controller.use(express.json());
        this.controller.use(express.urlencoded({ extended: true }));

        this.initControllers();

        this.controller.listen(appConfig.httpPort, () => {
            console.log({
                info: 'http server',
                status: 'launched',
            });
        });
    }

    private initControllers(): void {
        const commonController = new Common(this.controller);
        commonController.run();

        const webHookController = new WebHook(this.controller);
        webHookController.run();
    }
}

export default new Kernel();
