import { Client, Intents } from 'discord.js';
import { Database, open } from 'sqlite';
import sqlite3 from 'sqlite3';

import { config as DotenvConfig } from 'dotenv';

export interface AppConfig {
    botToken: string;
    guildId: string;
    notificationChannelId: string;
    gitlabToken: string;
    gitlabWebHookToken: string;
    gitlabHost: string;
    databasePath: string;
    checkMRInterval: number;
    httpPort: number;
}

export const DEFAULT_BUSY_TIMEOUT = 10000;

export class App {
    protected static config: AppConfig | null = null;
    protected static db?: Database;
    protected static botClient?: Client;

    constructor() {
        DotenvConfig();
    }

    public getAppConfig(): AppConfig {
        if (App.config !== null) {
            return App.config;
        }

        App.config = {
            botToken: process.env.BOT_TOKEN || '',
            guildId: process.env.GUILD_ID || '',
            notificationChannelId: process.env.NOTIFICATION_CHANNEL_ID || '',
            gitlabToken: process.env.GITLAB_TOKEN || '',
            gitlabWebHookToken: process.env.GITLAB_WEBHOOK_TOKEN || '',
            gitlabHost: process.env.GITLAB_HOST || 'https://gitlab.com',
            databasePath: process.env.DB_PATH || './data/db.db',
            checkMRInterval: Number(process.env.CHCK_MR_INTERVAL) || 10,
            httpPort: Number(process.env.HTTP_PORT) || 8080,
        };

        return App.config;
    }

    public async getDatabaseConnection(): Promise<Database> {
        if (App.db) {
            return App.db;
        }

        const appConfig = this.getAppConfig();

        const db = await open({
            filename: appConfig.databasePath,
            driver: sqlite3.Database,
        });

        await db.exec(`PRAGMA busy_timeout = ${DEFAULT_BUSY_TIMEOUT};`);

        App.db = db;

        return App.db;
    }

    public getBotClient(): Client {
        if (!App.botClient) {
            App.botClient = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });
        }

        return App.botClient;
    }
}

export default new App();
