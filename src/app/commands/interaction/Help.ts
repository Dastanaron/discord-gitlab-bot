import { CommandInteraction, Interaction, ApplicationCommandData, MessageEmbed } from 'discord.js';
import { AbstractInteractionCommand } from './AbstractInteractionCommand';

export class Help extends AbstractInteractionCommand {
    isValid(interaction: Interaction): boolean {
        return interaction.isCommand() && interaction.commandName === 'help';
    }

    async handle(interaction: CommandInteraction): Promise<void> {
        const message = new MessageEmbed();
        message
            .setTitle('Message commands')
            .addFields(
                { name: '!get channelId', value: 'Show Id this channel' },
                {
                    name: '!map usernames {gitlabUsername}-@{discordUsername}',
                    value: 'Map discord username with gitlab user name, example `!map usernames gitlabusername-@discrodUser`',
                },
                { name: '!show mr {gitlabUsername}', value: 'Show merge requests for user' },
            )
            .setTimestamp();
        await interaction.reply({ embeds: [message] });
    }

    createCommand(): ApplicationCommandData {
        return {
            type: 'CHAT_INPUT',
            name: 'help',
            description: 'Show all message commands',
        };
    }
}
