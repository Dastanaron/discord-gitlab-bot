import { CommandInteraction, Interaction, ApplicationCommandData, Permissions } from 'discord.js';
import { AbstractInteractionCommand } from './AbstractInteractionCommand';

export class ClearChannel extends AbstractInteractionCommand {
    isValid(interaction: Interaction): boolean {
        return interaction.isCommand() && interaction.commandName === 'clearchannel';
    }

    async handle(interaction: CommandInteraction): Promise<void> {
        const channel = this.client.channels.cache.find((ch) => ch.id === interaction.channelId);
        if (!channel?.isText()) {
            return;
        }

        if (
            !interaction.member ||
            !(interaction.member.permissions as Permissions).has(Permissions.FLAGS.MANAGE_MESSAGES)
        ) {
            await interaction.reply(`You don't have permission for delete messages`);
            return;
        }

        const messages = await channel.messages.fetch();

        if (messages) {
            messages.each(async (element) => {
                try {
                    await channel.messages.delete(element);
                } catch (e) {
                    //some messages cannot be deleted, because they are discord system messages
                    console.error(e);
                }
            });
        }

        await interaction.reply(`Messages was cleaned`);
    }

    createCommand(): ApplicationCommandData {
        return {
            type: 'CHAT_INPUT',
            name: 'clearchannel',
            description: 'Clear messages in this channel',
        };
    }
}
