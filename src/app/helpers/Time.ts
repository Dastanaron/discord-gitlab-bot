export class TimeHelper {
    public static dateToInternalFormat(rfcTime: string, withTimeZone = false): string {
        const date = new Date(rfcTime);

        const year = date.getFullYear();
        const month = (date.getMonth() + 1).toString().padStart(2, '0');
        const day = date.getDate().toString().padStart(2, '0');
        const hours = date.getHours().toString().padStart(2, '0');
        const minutes = date.getMinutes().toString().padStart(2, '0');
        const seconds = date.getSeconds().toString().padStart(2, '0');

        let time = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;

        if (withTimeZone) {
            const timezone = date.getTimezoneOffset() / -60;
            time += ` (GMT ${timezone > 0 ? '+' + timezone : '-' + timezone})`;
        }

        return time;
    }
}
