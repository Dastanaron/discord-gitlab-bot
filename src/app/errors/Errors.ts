import { StandardObject } from '../../contracts/ObjectTypes';

export abstract class BaseError extends Error {
    public errorCode: string;
    public context: StandardObject | undefined;

    protected constructor(message: string, errorCode: string, context?: StandardObject) {
        super(message);

        this.errorCode = errorCode;
        this.name = 'BaseError';
        this.context = context;

        Object.setPrototypeOf(this, new.target.prototype);
    }
}

export class HTTPError extends BaseError {
    public statusCode: number;

    constructor(message: string, statusCode: number, context?: StandardObject) {
        super(message, statusCode.toString(), context);
        this.name = 'HTTPError';
        this.statusCode = statusCode;
    }
}
