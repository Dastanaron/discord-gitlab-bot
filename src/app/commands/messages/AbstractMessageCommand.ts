import { Client, Message } from 'discord.js';

export abstract class AbstractMessageCommand {
    protected client: Client;

    constructor(client: Client) {
        this.client = client;
    }

    abstract isValid(message: Message): boolean;

    abstract handle(message: Message): Promise<void>;
}
