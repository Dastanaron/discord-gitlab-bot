import { Request } from 'express';
import App, { AppConfig } from '../../../App';
import { Client } from 'discord.js';
import { MergeRequest as MergeRequestEntity } from '../Entities/MergeRequest';
import { MapUsernames } from '../../../../database/Repositories/MapUsernames';

export abstract class AbstractHandler {
    protected appConfig: AppConfig;
    protected botClient: Client;

    constructor() {
        this.appConfig = App.getAppConfig();
        this.botClient = App.getBotClient();
    }

    abstract handle(request: Request): void;

    protected async getAssigneeUser(username: string): Promise<string> {
        const db = await App.getDatabaseConnection();

        const repository = new MapUsernames(db);

        const mappedUser = await repository.getByGitlabUsername(username);

        let assignee = username;

        if (mappedUser) {
            assignee = mappedUser.discord_username;
        }

        return assignee;
    }
}
