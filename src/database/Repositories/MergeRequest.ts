import { AbstractRepository } from './AbstractRepository';
import { MergeRequest } from '../Entities/MergeRequest';

export const TABLE_NAME = 'merge_requests';

export class MergeRequestRepository extends AbstractRepository {
    get tableName(): string {
        return TABLE_NAME;
    }

    async getByExternalId(externalId: number): Promise<MergeRequest | null> {
        const mr = await this.db.get(`select * from ${TABLE_NAME} where external_id = :id`, { ':id': externalId });
        if (!mr) {
            return null;
        }
        return mr;
    }

    async saveMR(mr: Partial<MergeRequest>): Promise<number> {
        const clonedTask = Object.assign({}, mr);

        if (!clonedTask.id) {
            delete clonedTask.id;
        }

        return super.save(mr, Object.keys(clonedTask));
    }
}
