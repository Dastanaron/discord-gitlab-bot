CREATE TABLE "merge_requests"
(
    "id"          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "external_id" INTEGER NOT NULL UNIQUE,
    "author"      TEXT    NOT NULL,
    "title"       TEXT    NOT NULL,
    "url"         TEXT    NOT NULL,
    "created_at"  INTEGER NOT NULL,
    "is_sent"     INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE "users_map"
(
    "id"               INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    "gitlab_username"  TEXT    NOT NULL UNIQUE,
    "discord_username" TEXT    NOT NULL UNIQUE
)
