import { AbstractAPI } from './AbstractApi';

export class Common extends AbstractAPI {
    public run(): void {
        this.apiMain();
    }

    private apiMain() {
        this.controller.get('/api/', (request, response) => {
            this.successResponse(response, undefined, 'It is Working!)');
        });
    }
}
