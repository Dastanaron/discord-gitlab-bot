import { CommandInteraction, Interaction, ApplicationCommandData } from 'discord.js';
import { AbstractInteractionCommand } from './AbstractInteractionCommand';

export class ShowRegisteredCommands extends AbstractInteractionCommand {
    isValid(interaction: Interaction): boolean {
        return interaction.isCommand() && interaction.commandName === 'showcommands';
    }

    async handle(interaction: CommandInteraction): Promise<void> {
        const commands = await this.client.application?.commands.fetch(undefined, {
            guildId: interaction.guildId || '',
        });

        let message = '';

        if (commands) {
            commands.each(async (element) => {
                message += `${element.name} - ${element.id}\n`;
            });
        } else {
            message = 'Not found registered commands';
        }

        await interaction.reply(message);
    }

    createCommand(): ApplicationCommandData {
        return {
            type: 'CHAT_INPUT',
            name: 'showcommands',
            description: 'Show registered commands in this server',
        };
    }
}
