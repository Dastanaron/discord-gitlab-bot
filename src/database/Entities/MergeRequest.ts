export interface MergeRequest {
    id: number;
    external_id: number;
    author: string;
    title: string;
    url: string;
    created_at: number;
    is_sent: number;
}
