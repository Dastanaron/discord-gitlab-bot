import { Response, Request, Express, NextFunction } from 'express';
import { StandardObject } from '../../contracts/ObjectTypes';
import { ResponseBody } from '../contracts/Http/Response';

export abstract class AbstractAPI {
    protected readonly controller: Express;

    constructor(controller: Express) {
        this.controller = controller;
    }

    public errorResponse(response: Response, message: string, code = 400): void {
        response.status(code);
        response.json({
            status: 'error',
            message: message,
        });
    }

    public successResponse(response: Response, data?: StandardObject, message?: string): void {
        response.status(200);
        response.json({
            status: 'ok',
            data: data,
            message: message,
        });
    }

    public responseByBody<T>(response: Response, body: ResponseBody<T>): void {
        response.status(body.code);
        response.json({
            status: body.isError ? 'error' : 'ok',
            data: body.data,
        });
    }

    public abstract run(): void;
}
