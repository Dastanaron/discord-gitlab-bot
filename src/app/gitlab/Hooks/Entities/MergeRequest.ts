import { Project } from './Project';
import { Repository } from './Repository';
import { Label } from './Label';

export interface Assignee {
    name: string;
    username: string;
    avatar_url: string;
    email: string;
}

export interface MergeRequest {
    object_kind: string;
    user: {
        id: number;
        name: string;
        username: string;
        avatar_url: string;
        email: string;
    };
    project: Project;
    repository: Repository;
    object_attributes: {
        id: number;
        target_branch: string;
        source_branch: string;
        source_project_id: number;
        author_id: number;
        assignee_id: number;
        title: string;
        created_at: string;
        updated_at: string;
        milestone_id: number;
        state: string;
        merge_status: string;
        target_project_id: number;
        iid: number;
        description: string;
        source: {
            name: string;
            description: string;
            web_url: string;
            avatar_url: string;
            git_ssh_url: string;
            git_http_url: string;
            namespace: string;
            visibility_level: number;
            path_with_namespace: string;
            default_branch: string;
            homepage: string;
            url: string;
            ssh_url: string;
            http_url: string;
        };
        target: {
            name: string;
            description: string;
            web_url: string;
            avatar_url: string;
            git_ssh_url: string;
            git_http_url: string;
            namespace: string;
            visibility_level: number;
            path_with_namespace: string;
            default_branch: string;
            homepage: string;
            url: string;
            ssh_url: string;
            http_url: string;
        };
        last_commit: {
            id: string;
            message: string;
            timestamp: string;
            url: string;
            author: {
                name: string;
                email: string;
            };
        };
        work_in_progress: boolean;
        url: string;
        action: string;
        assignee?: Assignee;
    };
    labels: Label[];
    changes: {
        updated_by_id: {
            previous: number;
            current: number;
        };
        updated_at: {
            previous: string;
            current: string;
        };
        labels: {
            previous: Label[];
            current: Label[];
        };
    };
    assignees: Assignee[];
}
