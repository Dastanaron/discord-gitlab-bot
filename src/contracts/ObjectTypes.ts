export interface StandardObject {
    [key: string]: any;
}

export interface SimpleObject {
    [key: string]: number | string;
}
