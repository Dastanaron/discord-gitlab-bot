import { ApplicationCommandData, Client, Interaction } from 'discord.js';

export abstract class AbstractInteractionCommand {
    protected client: Client;

    constructor(client: Client) {
        this.client = client;
    }

    abstract isValid(interaction: Interaction): boolean;

    abstract handle(interaction: Interaction): Promise<void>;

    abstract createCommand(): ApplicationCommandData;
}
