import { AbstractHandler } from './AbstractHandler';
import { Request } from 'express';
import { Push as PushEntity } from '../Entities/Push';
import { MessageEmbed, EmbedFieldData } from 'discord.js';

export class Push extends AbstractHandler {
    async handle(request: Request): Promise<void> {
        const channel = this.botClient.channels.cache.find((ch) => ch.id === this.appConfig.notificationChannelId);
        if (!channel?.isText()) {
            console.error('Undefined notification channel');
            return;
        }

        await channel.send({ embeds: [await this.buildMessage(request.body)] });
    }

    private async buildMessage(body: PushEntity): Promise<MessageEmbed> {
        const fields: EmbedFieldData[] = [];

        for (const commit of body.commits) {
            fields.push({
                name: commit.title,
                value: `[view commit](${commit.url})`,
            });
        }

        return new MessageEmbed()
            .setColor('#a246ff')
            .setTitle(body.project.name)
            .setURL(body.project.web_url)
            .setAuthor(body.user_name, body.user_avatar)
            .setDescription(`Push to repository ${body.repository.name}`)
            .setThumbnail(body.project.avatar_url)
            .addFields(fields)
            .setTimestamp()
            .setFooter(`Total commits: ${body.total_commits_count}`);
    }
}
