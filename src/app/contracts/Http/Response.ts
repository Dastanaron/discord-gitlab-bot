export interface ResponseBody<T> {
    isError: boolean;
    code: number;
    data?: T;
}
