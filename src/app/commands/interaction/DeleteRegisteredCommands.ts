import { CommandInteraction, Interaction, ApplicationCommandData, Permissions } from 'discord.js';
import { AbstractInteractionCommand } from './AbstractInteractionCommand';

export class DeleteRegisteredCommands extends AbstractInteractionCommand {
    isValid(interaction: Interaction): boolean {
        return interaction.isCommand() && interaction.commandName === 'clearcommands';
    }

    async handle(interaction: CommandInteraction): Promise<void> {
        const commands = await this.client.application?.commands.fetch(undefined, {
            guildId: interaction.guildId || '',
        });

        if (
            !interaction.member ||
            (interaction.member.permissions as Permissions).has(Permissions.FLAGS.MANAGE_GUILD)
        ) {
            await interaction.reply(`You don't have permission for command controlling`);
            return;
        }

        let i = 0;
        if (commands) {
            commands.each(async (element) => {
                await this.client.application?.commands.delete(element.id, interaction.guildId || '');
                i++;
            });
        }

        await interaction.reply(`Deleted ${i} commands`);
    }

    createCommand(): ApplicationCommandData {
        return {
            type: 'CHAT_INPUT',
            name: 'clearcommands',
            description: 'Clear registered commands in this server',
        };
    }
}
