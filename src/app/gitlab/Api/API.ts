import fetch from 'node-fetch';
import App from '../../App';
import { MergeRequest } from './Entities/MergeRequest';

export class GitLabAPI {
    protected host: string;
    protected token: string;

    constructor() {
        const appConfig = App.getAppConfig();
        this.host = appConfig.gitlabHost;
        this.token = appConfig.gitlabToken;
    }

    public async loadMergeRequests(state = 'opened'): Promise<MergeRequest[]> {
        const url = this.host + '/api/v4/merge_requests?state=' + state;

        return (await fetch(url, {
            method: 'GET',
            headers: {
                'private-token': this.token,
            },
        }).then((res) => res.json())) as MergeRequest[];
    }
}

export default new GitLabAPI();
