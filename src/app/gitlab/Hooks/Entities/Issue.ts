import { Project } from './Project';
import { Label } from './Label';

export interface Issue {
    object_kind: string;
    event_type: string;
    user: {
        id: number;
        name: string;
        username: string;
        avatar_url: string;
        email: string;
    };
    project: Project;
    object_attributes: {
        id: number;
        title: string;
        assignee_ids: number[];
        assignee_id: number;
        author_id: number;
        project_id: number;
        created_at: string;
        updated_at: string;
        updated_by_id: number;
        last_edited_at: number;
        last_edited_by_id: number;
        relative_position: number;
        description: string;
        milestone_id: number;
        state_id: number;
        confidential: boolean;
        discussion_locked: boolean;
        due_date: number;
        moved_to_id: number;
        duplicated_to_id: number;
        time_estimate: number;
        total_time_spent: number;
        time_change: number;
        human_total_time_spent: number;
        human_time_estimate: number;
        human_time_change: number;
        weight: number;
        iid: number;
        url: string;
        state: string;
        action: string;
        labels: Label[];
    };
    repository: {
        name: string;
        url: string;
        description: string;
        homepage: string;
    };
    assignees: [
        {
            name: string;
            username: string;
            avatar_url: string;
        },
    ];
    assignee: {
        name: string;
        username: string;
        avatar_url: string;
    };
    labels: Label[];
    changes: {
        updated_by_id: {
            previous: number;
            current: number;
        };
        updated_at: {
            previous: string;
            current: string;
        };
        labels: {
            previous: Label[];
            current: Label[];
        };
    };
}
