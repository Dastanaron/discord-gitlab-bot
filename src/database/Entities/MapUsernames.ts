export interface MapUsernames {
    id: number;
    gitlab_username: string;
    discord_username: string;
}
