import { Message } from 'discord.js';
import { AbstractMessageCommand } from './AbstractMessageCommand';

const COMMAND = '!get channelId';

export class GetChannelId extends AbstractMessageCommand {
    isValid(message: Message): boolean {
        return message.content === COMMAND;
    }

    async handle(message: Message): Promise<void> {
        await message.reply(`Channel ID: ${message.channelId}`);
    }
}
