import { Project } from './Project';
import { Repository } from './Repository';
import { Commit } from './Commit';

export interface Push {
    object_kind: string;
    before: string;
    after: string;
    ref: string;
    checkout_sha: string;
    user_id: number;
    user_name: string;
    user_username: string;
    user_email: string;
    user_avatar: string;
    project_id: number;
    project: Project;
    repository: Repository;
    commits: Commit[];
    total_commits_count: number;
}
