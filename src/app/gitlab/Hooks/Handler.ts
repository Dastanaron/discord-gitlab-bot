import { Request } from 'express';
import { MergeRequest } from './Handlers/MergeRequest';
import App from '../../App';
import { HTTPError } from '../../errors/Errors';
import { Push } from './Handlers/Push';

export const PUSH_HOOK = 'Push Hook';
export const TAG_PUSH_HOOK = 'Tag Push Hook';
export const ISSUE_HOOK = 'Issue Hook';
export const MERGE_REQUEST_HOOK = 'Merge Request Hook';

export class Handler {
    public async run(request: Request): Promise<void> {
        const appConfig = App.getAppConfig();

        const eventType = request.header('X-Gitlab-Event');
        const token = request.header('X-Gitlab-Token');

        if (appConfig.gitlabWebHookToken !== '' && appConfig.gitlabWebHookToken !== token) {
            console.log('Wrong X-Gitlab-Token');
            throw new HTTPError('Wrong X-Gitlab-Token', 401);
        }

        if (!eventType) {
            console.log('Undefined X-Gitlab-Event header type');
            throw new HTTPError('Undefined X-Gitlab-Event header type', 422);
        }

        switch (eventType) {
            case PUSH_HOOK: {
                await new Push().handle(request);
                return;
            }
            case MERGE_REQUEST_HOOK: {
                await new MergeRequest().handle(request);
                return;
            }
            default: {
                console.error('Undefined event type: ' + eventType);
                return;
            }
        }
    }
}

export default new Handler();
