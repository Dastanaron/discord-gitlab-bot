import { Message } from 'discord.js';
import { AbstractMessageCommand } from './AbstractMessageCommand';

export class Hello extends AbstractMessageCommand {
    isValid(message: Message): boolean {
        return message.content === 'hi';
    }

    async handle(message: Message): Promise<void> {
        await message.reply(`Hello ${message.author.username} :wave:`);
    }
}
