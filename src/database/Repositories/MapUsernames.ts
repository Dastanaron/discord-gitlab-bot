import { AbstractRepository } from './AbstractRepository';
import { MapUsernames as MapUsernamesEntity } from '../Entities/MapUsernames';

export const TABLE_NAME = 'users_map';

export class MapUsernames extends AbstractRepository {
    get tableName(): string {
        return TABLE_NAME;
    }

    async getByGitlabUsername(username: string): Promise<MapUsernamesEntity | null> {
        const mappedUser = await this.db.get(`select * from ${TABLE_NAME} where gitlab_username = :username`, {
            ':username': username,
        });
        if (!mappedUser) {
            return null;
        }
        return mappedUser;
    }

    async saveMap(entity: Partial<MapUsernamesEntity>): Promise<number> {
        const clonedTask = Object.assign({}, entity);

        if (!clonedTask.id) {
            delete clonedTask.id;
        }

        return super.save(entity, Object.keys(clonedTask));
    }
}
